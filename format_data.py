import csv

import numpy as np


def sort_csv_by_time_track(csv_file):
    csv_file = sorted(csv_file, key=lambda row: (row[0], row[1]), reverse=True)
    return csv_file


def format_csv_to_vector(path_to_scv):
    dict_of_vectors = {}
    reader = csv.reader(open(path_to_scv), delimiter=";")
    reader = sort_csv_by_time_track(reader)
    for idx in range(len(reader) - 1):
        track = reader[idx][0]
        if track == reader[idx + 1][0]:
            begin = int(reader[idx + 1][2]) - int(reader[idx][2])
            end = int(reader[idx + 1][3]) - int(reader[idx][3])
            dot = [np.array([int(reader[idx][2]), int(reader[idx][3])]),
                   np.array([int(reader[idx + 1][2]), int(reader[idx + 1][3])])]

            vc = np.array([begin, end])
            if track not in dict_of_vectors.keys():
                dict_of_vectors[track] = []

            dict_of_vectors[track].append((vc, dot))

    return dict_of_vectors
