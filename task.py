import numpy as np

from format_data import format_csv_to_vector


def unit_vector(vector):
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.degrees(np.arccos(np.dot(v1_u, v2_u)))


def distance_vectors(v1, v2):
    begin_point_distant = np.sqrt(np.sum(np.square(v1[0] - v2[0])))
    end_point_distant = np.sqrt(np.sum(np.square(v1[1] - v2[1])))
    return abs(end_point_distant - begin_point_distant)


def check_matching(list_of_v1, list_of_v2):
    counter_matches = 0

    for vc_1 in list_of_v1:
        for vc_2 in list_of_v2:
            if distance_vectors(vc_1[1], vc_2[1]) < 20:
                if angle_between(vc_1[0], vc_2[0]) < 15:
                    counter_matches += 1
                    break

    return counter_matches / len(list_of_v1)


if __name__ == '__main__':
    dict_of_vectors = format_csv_to_vector("traks.csv")
    list_of_track = list(dict_of_vectors.keys())
    list_of_track = [(a, b) for idx, a in enumerate(list_of_track) for b in list_of_track[idx + 1:]]

    for pair in list_of_track:
        list_of_v1, list_of_v2 = sorted([dict_of_vectors[pair[0]], dict_of_vectors[pair[1]]], key=lambda el: len(el))
        res = check_matching(list_of_v1, list_of_v2)

        if res < 0.62:
            print("существенное отличие траектории {} от траектории {}.".format(*pair))
        elif res > 0.81:
            print("совпадение траектории {} и траектории {}.".format(*pair))
        else:
            print("незначительное отличие траектории {} от траектории {}.".format(*pair))
